<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $connection = 'oracle';
    protected $table = 'perfil';
    protected $fillable = [
        'cd_perfil','ds_perfil','ie_situacao','dt_atauzalicao'

    ];
}
