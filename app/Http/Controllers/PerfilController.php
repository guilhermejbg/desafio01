<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Perfil;
use  Illuminate\Database\Eloquent\Collection;

class PerfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        //$perfil=DB::table('perfil')->get(); //Não entendi o motivo de esta chamando o DB. Pra que a Model então?
        $perfil=Perfil::paginate(15);;
        /*$perfil=Perfil::all()->paginate(15);*/


            //return view('usuarios.perfil', [
                //'perfil' => DB::table('perfil')->paginate(50);//Não entendi o motivo de esta chamando o DB. Pra que a Model então?
                //'perfil' => $perfil, //Não precisava fazer a mesma consulta 2 vezes
            //]);


            return view("usuarios.perfil", compact("perfil")); //Você pode chamar a view com o uso do "compact".
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
