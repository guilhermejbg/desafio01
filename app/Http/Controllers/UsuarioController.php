<?php
namespace App\Http\Controllers;
use App\Http\Requests\StoreUpdateUsuarioRequest;
use App\Usuario;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


/**
 * identar código é importante!
 */
class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $usuario;
    public function __construct(Usuario $usuario){
$this ->usuario = $usuario;

    }
    public function index()
         
    {
       //estude sobre o seguinte assunto: injeção de dependência
       $usuarios = $this ->usuario->all();
       return view('usuarios.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StoreUpdateUsuarioRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateUsuarioRequest $request)
    {

        try {
            //estude sobre o seguinte assunto: injeção de dependência
            $usuario = new Usuario();
            $usuario->nome = $request->nome;
            $usuario->email = $request->email;
            $usuario->save();

            $usuarios = Usuario::all();
            Flash('Usuario Cadastrado com sucesso!!!')->success();
            return view('usuarios.index', compact('usuarios'));
        } catch (\Throwable $th) {
            //Não entendi o motivo de qualquer erro ser "email já cadastrado".
            //Faça consistências dentro da Request!!!
            Flash('Ops!! Ocorreu um erro')->error();
            return redirect()->route('Usuario.index');
        

        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            //estude sobre o seguinte assunto: injeção de dependência
            $usuario = Usuario::findOrFail($id);
            return view('usuarios.delete', compact('usuario'));
        } catch (\Exception $e) {

            $usuarios = Usuario::all();
            Flash('Ops!!. Ocorreu um erro O Usuário não Existe')->error();
           //return view('usuarios.create');
            //O ideal é você utilizar rotas já existentes!
            //return view('usuarios.index', compact('usuarios'));
            return redirect()->route('Usuario.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      try {
        $usuario = Usuario::findOrFail($id);
        return view('usuarios.edit', compact('usuario'));
      } catch (\Throwable $th) {
              Flash('Ops!! Não é possivel editar Usuario não existe')->error();
        return redirect()->route('Usuario.index');
      }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\StoreUpdateUsuarioRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateUsuarioRequest $request, $id)
    {
    try {
        //estude sobre o seguinte assunto: injeção de dependência
        $usuario = Usuario::findOrFail($id);
        $usuario->nome = $request->nome;
        $usuario->email = $request->email;
        $usuario->save();
      
        Flash('Usuario Atualizado com sucesso!!!')->success();

        return redirect()->route('Usuario.index'); //Usar rotas já existente!!!
        //return view('usuarios.index', compact('usuarios'));
    } catch (\Throwable $th) {
          $usuario = Usuario::findOrFail($id);
          Flash('Ops!! Algo deu errado tente novamente')->error();
          return view('usuarios.edit');

    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            //estude sobre o seguinte assunto: injeção de dependência
            $usuario = Usuario::findOrFail($id);
            $usuario ->delete();

            $usuarios = Usuario::all();
            Flash('Usuario Deletado com sucesso!!!')->warning();
            return view('usuarios.index', compact('usuarios'));
        } catch (\Throwable $th) {
            $usuarios = Usuario::all();
            return redirect()->route('Usuario.index');
        }
    }
}
