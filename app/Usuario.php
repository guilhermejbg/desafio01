<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{ 
    protected $connection = 'mysql';
    protected $fillable = [
        'nome', 'email'];
}
