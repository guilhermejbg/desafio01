<?php
use App\Usuario;
use Illuminate\Support\Facades\Route;



Route::get('/', function () {
 return view('usuarios.inicio');
});

Route::resource('Usuario','UsuarioController');
Route::resource('Perfil','PerfilController');
