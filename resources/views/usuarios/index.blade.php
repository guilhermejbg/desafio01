
@extends('usuarios.app')   
@section('title','Usuarios')
@section('content')


<h1 class = 'text-center'>Usuarios</h1>
    <a href="{{route('Usuario.create')}}" class ='btn btn-success my-2' >Novo+</a>
    
    @include('flash::message')
   
    <table class="table table-striped table-hover ">

        <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Email</th>
                <th width = '200'>Ações</th>
               
            </tr>
        </thead>
        <tbody>
        @foreach($usuarios as $usuario)
            <tr>
                <th>{{$usuario->id}}</th>
                <th>{{$usuario->nome}}</th>
                <th>{{$usuario->email}}</th>

                
                

                <th>
                    <form action="{{route('Usuario.destroy',$usuario->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <a href="{{route('Usuario.edit',$usuario->id)}}" class="btn btn-primary">Editar</a>
                    <button type="submit"  class="btn btn-danger">Excluir</button>
                    </form>
                </th>

            </tr>

        @endforeach
        </tbody>
    </table>
    @endsection