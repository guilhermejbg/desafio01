@extends('usuarios.app')
@section('title','Cadastro')
@section('content')
@include('flash::message')

@if($errors->any())
<div>
    <ul>
    @foreach ($errors->all() as $messagem)
    <li>{{$messagem}}</li>
    @endforeach
    </ul>
</div>   
@endif    
<h1 class="text-center">Criar um novo Usuario</h1>
<div id="criar">
 
    <form action="{{route('Usuario.store')}}" method="POST">
      @csrf
         <div class="col-form-label">
          <label for="exampleInputEmail1" class="form-label">Nome</label>
          <br>
          <input type="text" class="col-auto" id="label" name="nome">
          <div id="emailHelp" class="form-text"></div>
        </div>
        <div class="col-form-label">
          <label  for="exampleInputPassword1" class="form-label">Email</label>
          <br>
          <input type="email" class="col-auto" id="label" name="email">
        </div>
        
        <button type="submit" class="btn btn-primary">Cadastrar</button>
      </form>

    </div>
    @endsection 
