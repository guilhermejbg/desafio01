@extends('usuarios.app')

@section('title','Editar Produtos')
@section('content')
    
@include('flash::message')
    @if($errors->any())
<div class = "alert alert-danger">
    <ul>
    @foreach ($errors->all() as $messagem)
    <li>{{$messagem}}</li>
    @endforeach
    </ul>
</div>   
@endif    
    <h1 class="text-center">editar Usuario</h1>
<div id ="criar">
    <form action="{{route('Usuario.update',$usuario->id)}}" method="post">
        @method('PUT')
        @CSRF
        <div class="col-form-label">
            <label for="exampleInputEmail1" class="form-label">Nome</label>
            <br>
            <input type="text" class="col-auto" id="label" name="nome" value="{{$usuario->nome}}">
            <div id="emailHelp" class="form-text"></div>
          </div>
          <div class="col-form-label">
            <label  for="exampleInputPassword1" class="form-label">Email</label>
            <br>
            <input type="email" class="col-auto" id="label" name="email" value="{{$usuario->nome}}">
            <br>
        <button class="btn btn-primary">Editar</button>
        
    </div>
    </form>
    @endsection