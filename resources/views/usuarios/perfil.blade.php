
@extends('usuarios.app')   
@section('title','Perfil')
@section('content')
    

<h1 class = 'text-center'>Perfis Cadastrados no Tasy</h1>
    @include('flash::message')
   
    <table class="table table-striped table-hover ">

        <thead>
            <tr>
                <th>Cod.Perfil</th>
                <th>Perfil</th>
                <th>status</th>
               
            </tr>
        </thead>
        <tbody>
        @foreach($perfil as $perfils)
            <tr>
                <th>{{$perfils->cd_perfil}}</th>
                <th>{{$perfils->ds_perfil}}</th>
                <th>{{$perfils->ie_situacao}}</th>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $perfil->links() }}
    @endsection